package pl.rso.webservice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("beers")
@Produces(MediaType.APPLICATION_JSON)
public class WebserviceREST {

    @Inject
    private BeerFacade beerFacade;

    private Gson gson = new GsonBuilder().create();

    @GET
    public Response getAll()
    {
        List<Beer> beerList = beerFacade.getAll();
        return Response.ok().entity(gson.toJson(beerList)).build();
    }

    @GET
    @Path("add")
    public Response persist(
            @QueryParam("name") String name,
            @QueryParam("price") Double price
    )
    {
        Beer beer = new Beer();
        beer.setName(name);
        beer.setPrice(price);
        beerFacade.persist(beer);
        return Response.status(201).entity(beer).build();
    }

}

package pl.rso.webservice;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "Beers")

@NamedQuery(
        name="findAll", query="SELECT b FROM Beer b"
)
@Data
@Setter
@Getter
public class Beer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private Double price;

}

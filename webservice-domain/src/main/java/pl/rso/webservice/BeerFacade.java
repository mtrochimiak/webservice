package pl.rso.webservice;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

public class BeerFacade {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void persist(Beer beer)
    {
        entityManager.persist(beer);
    }

    public List<Beer> getAll()
    {
        return entityManager.createNamedQuery("findAll").getResultList();
    }

}
